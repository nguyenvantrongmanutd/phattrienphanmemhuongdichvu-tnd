/*
Navicat MySQL Data Transfer

Source Server         : TEST
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : QLCVGT

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2018-11-02 23:21:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for DEPARTMENT
-- ----------------------------
DROP TABLE IF EXISTS `DEPARTMENT`;
CREATE TABLE `DEPARTMENT` (
  `DEPARTMENT_ID` int(11) NOT NULL,
  `DEPARTMENT_NAME` text,
  `DEPARTMENT_CREATED_BY` text,
  `DEPARTMENT_UPDATED_BY` text,
  PRIMARY KEY (`DEPARTMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of DEPARTMENT
-- ----------------------------

-- ----------------------------
-- Table structure for DOCUMENT
-- ----------------------------
DROP TABLE IF EXISTS `DOCUMENT`;
CREATE TABLE `DOCUMENT` (
  `DOCUMENT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOCUMENT_TITLE` text,
  `DOCUMENT_TYPE` text,
  `DOCUMENT_TYPE_DETAIL` text,
  `DOCUMENT_URL` text,
  `DOCUMENT_STATUS` int(11) DEFAULT NULL,
  `DOCUMENT_DESCRIPTION` text,
  `DOCUMENT_CREATED_BY_ID` int(11) DEFAULT NULL,
  `DOCUMENT_CREATED_BY` text,
  `DOCUMENT_CREATED_TIME` date DEFAULT NULL,
  `DOCUMENT_UPDATED_BY` date DEFAULT NULL,
  `USER_SENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of DOCUMENT
-- ----------------------------
INSERT INTO `DOCUMENT` VALUES ('1', 'Bao cao tai chinh', '1', null, null, '3', null, null, 'user1', '2018-10-11', null, null);
INSERT INTO `DOCUMENT` VALUES ('2', 'Bao cao vui', '1', null, null, '3', null, null, 'user1', '2018-10-03', null, null);
INSERT INTO `DOCUMENT` VALUES ('3', 'Tai lieu noi bo', '1', null, null, '3', null, null, 'user2', '2018-10-17', null, null);
INSERT INTO `DOCUMENT` VALUES ('4', 'Tai lieu linh tinh', '1', null, null, '3', null, null, 'user2', '2018-10-03', null, null);
INSERT INTO `DOCUMENT` VALUES ('5', 'ha', 'ha', null, 'ha', '3', 'ha', null, 'user2', '2018-10-12', null, null);
INSERT INTO `DOCUMENT` VALUES ('6', 'd61f41bf84bffff351bb734c8fa714be.jpg', 'Vui', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/d61f41bf84bffff351bb734c8fa714be.jpg', '3', null, null, 'user2', '2018-10-12', null, null);
INSERT INTO `DOCUMENT` VALUES ('7', 'Screenshot from 2018-09-21 23-48-37.png', 'Doc', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-21 23-48-37.png', '0', null, null, 'user2user2', '2018-10-25', null, null);
INSERT INTO `DOCUMENT` VALUES ('8', 'Screenshot from 2018-09-10 09-51-42.png', 'Doc', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-10 09-51-42.png', '0', null, null, 'user2', '2018-10-11', null, null);
INSERT INTO `DOCUMENT` VALUES ('9', 'Screenshot from 2018-09-29 22-38-22.png', 'Doc', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-29 22-38-22.png', '0', null, null, 'user2', '2018-10-10', null, null);
INSERT INTO `DOCUMENT` VALUES ('10', 'Screenshot from 2018-07-30 10-47-24.png', 'Doc', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-07-30 10-47-24.png', '3', null, null, 'user2', '2018-09-05', null, null);
INSERT INTO `DOCUMENT` VALUES ('11', 'Screenshot from 2018-09-24 08-11-12.png', 'Doc', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-24 08-11-12.png', '3', null, null, 'user2', '2018-10-20', null, null);
INSERT INTO `DOCUMENT` VALUES ('12', 'Screenshot from 2018-07-30 10-47-24.png', 'Doc', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-07-30 10-47-24.png', '3', null, null, 'user2', '2018-10-13', null, null);
INSERT INTO `DOCUMENT` VALUES ('13', 'Screenshot from 2018-09-24 08-11-12.png', 'Doc', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-24 08-11-12.png', '0', null, null, 'user2', '2018-10-03', null, null);
INSERT INTO `DOCUMENT` VALUES ('17', 'Screenshot from 2018-09-10 09-48-06.png', 'FIX CUNG', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-10 09-48-06.png', '3', null, null, 'user1', '2018-10-20', null, null);
INSERT INTO `DOCUMENT` VALUES ('18', 'Screenshot from 2018-09-24 08-11-12.png', 'FIX CUNG', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-24 08-11-12.png', '3', null, null, 'user1', '2018-10-20', null, null);
INSERT INTO `DOCUMENT` VALUES ('19', '1620379_676868639099200_7774041982478045201_n.jpg', 'FIX CUNG', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/1620379_676868639099200_7774041982478045201_n.jpg', '0', null, null, 'user1', '2018-10-20', null, null);
INSERT INTO `DOCUMENT` VALUES ('20', 'Screenshot from 2018-07-30 10-47-24.png', 'FIX CUNG', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-07-30 10-47-24.png', '0', null, null, 'user1', '2018-10-20', null, null);
INSERT INTO `DOCUMENT` VALUES ('21', 'Screenshot from 2018-09-10 09-48-06.png', 'FIX CUNG', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-10 09-48-06.png', '0', null, null, 'user1', '2018-10-20', null, null);
INSERT INTO `DOCUMENT` VALUES ('22', 'Screenshot from 2018-09-21 23-48-37.png', 'FIX CUNG', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-21 23-48-37.png', '0', null, null, 'user1', '2018-10-20', null, null);
INSERT INTO `DOCUMENT` VALUES ('23', 'Screenshot from 2018-09-10 09-48-06.png', '1', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-10 09-48-06.png', '0', null, null, 'user1', '2018-10-24', null, null);
INSERT INTO `DOCUMENT` VALUES ('24', 'Screenshot from 2018-09-29 22-38-22.png', '1', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-29 22-38-22.png', '0', null, null, 'user1', '2018-10-24', null, null);
INSERT INTO `DOCUMENT` VALUES ('25', 'Screenshot from 2018-07-30 10-47-24.png', '1', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-07-30 10-47-24.png', '0', null, null, 'user1', '2018-10-24', null, null);
INSERT INTO `DOCUMENT` VALUES ('26', 'Screenshot from 2018-07-30 10-47-24.png', '1', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-07-30 10-47-24.png', '0', null, null, 'user1', '2018-10-24', null, null);
INSERT INTO `DOCUMENT` VALUES ('27', 'Screenshot from 2018-04-03 16-51-33.png', '1', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-04-03 16-51-33.png', '0', null, null, 'user1', '2018-10-24', null, null);
INSERT INTO `DOCUMENT` VALUES ('28', 'Screenshot from 2018-04-03 16-51-33.png', '1', null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-04-03 16-51-33.png', '0', null, null, 'user1', '2018-10-24', null, null);
INSERT INTO `DOCUMENT` VALUES ('35', 'Screenshot from 2018-07-30 10-47-24.png', null, null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-07-30 10-47-24.png', '0', null, null, 'user1', '2018-10-24', null, null);
INSERT INTO `DOCUMENT` VALUES ('36', 'Screenshot from 2018-09-21 23-48-37.png', null, null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/Screenshot from 2018-09-21 23-48-37.png', '0', null, null, 'user1', '2018-10-24', null, null);
INSERT INTO `DOCUMENT` VALUES ('37', 'index.html', null, null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/index.html', '0', null, null, null, null, null, null);
INSERT INTO `DOCUMENT` VALUES ('38', 'preview.swf', null, null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/preview.swf', '0', null, null, null, null, null, null);
INSERT INTO `DOCUMENT` VALUES ('39', 'config.xml', null, null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/config.xml', '0', null, null, null, null, null, null);
INSERT INTO `DOCUMENT` VALUES ('40', 'config.xml', null, null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/config.xml', '0', null, null, null, null, null, null);
INSERT INTO `DOCUMENT` VALUES ('41', 'index.html', null, null, '/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files/index.html', '0', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for DOCUMENT_TYPE
-- ----------------------------
DROP TABLE IF EXISTS `DOCUMENT_TYPE`;
CREATE TABLE `DOCUMENT_TYPE` (
  `DOCUMENT_TYPE_ID` int(10) NOT NULL,
  `DOCUMENT_TYPE_NAME` text,
  PRIMARY KEY (`DOCUMENT_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of DOCUMENT_TYPE
-- ----------------------------
INSERT INTO `DOCUMENT_TYPE` VALUES ('1', 'Ga');

-- ----------------------------
-- Table structure for DOCUMENT_TYPE_DETAIL
-- ----------------------------
DROP TABLE IF EXISTS `DOCUMENT_TYPE_DETAIL`;
CREATE TABLE `DOCUMENT_TYPE_DETAIL` (
  `DOCUMENT_TYPE_DETAIL_ID` int(255) NOT NULL,
  `DOCUMENT_TYPE_ID` text,
  `DOCUMENT_TYPE_DETAIL_NAME` text,
  PRIMARY KEY (`DOCUMENT_TYPE_DETAIL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of DOCUMENT_TYPE_DETAIL
-- ----------------------------
INSERT INTO `DOCUMENT_TYPE_DETAIL` VALUES ('1', '1', 'Chim');
INSERT INTO `DOCUMENT_TYPE_DETAIL` VALUES ('2', '1', 'Ga');
INSERT INTO `DOCUMENT_TYPE_DETAIL` VALUES ('3', '5', 'Coc');

-- ----------------------------
-- Table structure for USER
-- ----------------------------
DROP TABLE IF EXISTS `USER`;
CREATE TABLE `USER` (
  `USER_ID` int(11) NOT NULL,
  `USER_FIRSTNAME` text,
  `USER_LASTNAME` text,
  `USER_ROLE_ID` int(11) DEFAULT NULL,
  `DEPARTMENT_ID` int(11) DEFAULT NULL,
  `USER_CREATED_TIME` date DEFAULT NULL,
  `USER_STATUS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of USER
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
