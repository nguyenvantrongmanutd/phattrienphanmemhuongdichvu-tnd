<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@include file="/base/meta.jsp" %>
	<link href="css/index.css" rel="stylesheet">
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@include file="/base/header.jsp" %>

			<!-- page content -->
      <div class="right_col" role="main">
        <div class="row">
          <a type="button" href="#" id="delete-all" class="btn btn-default"><i class="fa fa-trash"></i>
            Xóa
          </a>
          
          <a type="button" href="#" id="download-all" class="btn btn-success"><i class="fa fa-download"></i>
            Tải Về
          </a>

          <!-- Button trigger modal -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-search-plus"></i>
            Tìm Kiếm Nâng Cao
          </button>

          <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">

                  <!-- form search -->
                  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Tên</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" id="first-name" placeholder="Tên Tài liệu" class="form-control input-sm col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-2 col-sm-2 col-xs-12" for="last-name">Nơi Gửi: </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control input-sm col-md-5">
                          <option>Phòng Tài Chính</option>
                          <option>Bộ Phận Văn Thư</option>
                          <option>Phòng Kỹ Thuật</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">Loại Tài Liệu: </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control input-sm col-md-5">
                          <option>Công Văn</option>
                          <option>Thông Báo</option>
                          <option>Báo Cáo</option>
                          <option>Tài Liệu</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">Ngày Gửi: </label>
                      <div class="col-sm-4">
                        <div class="input-group date datepicker" data-provide="datepicker">
                          <input type="text" class="form-control input-sm">
                          <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                          </div>
                        </div>
                      </div>

                      <label for="middle-name" class="control-label col-md-1 col-sm-1 col-xs-12">Tới: </label>
                      
                      <div class="col-sm-4">
                        <div class="input-group date datepicker" data-provide="datepicker">
                          <input type="text" class="form-control input-sm">
                          <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                          </div>
                        </div>
                      </div>
                    </div>

                  </form>
                
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Đóng</button>
                  <button type="button" id="test" class="btn btn-sm btn-primary">Tìm Kiếm</button>
                </div>
              </div>  
            </div>
          </div>

          <!-- Modal edit-->
          <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Chỉnh Sửa Tài Liệu</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                  <button type="button" id="test" class="btn btn-primary">Tìm Kiếm</button>
                </div>
              </div>
            </div>
          </div>


          <!-- datatable client -->
          <div class="x_panel">
            <div class="x_content">
              <table id="datatable" class="table table-striped table-sm table-bordered">
                <thead>
                  <tr>
                    <th><input type="checkbox" name="" id="check-all"></th>
                    <th>STT</th>
                    <th>Tiêu Đề</th>
                    <th>Nơi Gửi</th>
                    <th>Loại Công Văn</th>
                    <th>Ngày Gửi</th>
                    <th></th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>1</td>
                    <td>Báo cáo tài chính tháng 8</td>
                    <td>Phòng Kế Toán</td>
                    <td>Báo Cáo</td>
                    <td>2011/04/25</td>
                    <td>
                      
                      <button type="button" class="btn btn-default btn-sm" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>2</td>
                    <td>Thông báo lịch làm</td>
                    <td>Phòng Nhân Sự</td>
                    <td>Thông Báo</td>
                    <td>2011/04/25</td>
                    <td>

                      <button type="button" class="btn btn-default btn-sm" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>3</td>
                    <td>Tài liệu thiết kế</td>
                    <td>Phòng Kỹ Thuật</td>
                    <td>Tài Liệu</td>
                    <td>2011/04/25</td>
                    <td>
                      
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>
                  
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>4</td>
                    <td>Thông tin khách hàng A</td>
                    <td>Phòng Marketing</td>
                    <td>Tài Liệu</td>
                    <td>2011/04/25</td>
                    <td>
                      
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>     
                                 
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>4</td>
                    <td>Thông tin khách hàng B</td>
                    <td>Phòng Marketing</td>
                    <td>Tài Liệu</td>
                    <td>2011/04/25</td>
                    <td>
                      
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>
                                                     
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>5</td>
                    <td>file 6</td>
                    <td>Nguyễn Văn F</td>
                    <td>PNG</td>
                    <td>2011/04/25</td>
                    <td>
                      
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>
                                                     
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>6</td>
                    <td>file 6</td>
                    <td>Nguyễn Văn G</td>
                    <td>PNG</td>
                    <td>2011/04/25</td>
                    <td>
                      
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>
                                                     
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>7</td>
                    <td>file 4</td>
                    <td>Nguyễn Văn B</td>
                    <td>PNG</td>
                    <td>2011/04/25</td>
                    <td>
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>
                                                     
                  <tr>
                    <td><input type="checkbox" class="checkbox" name=""></td>
                    <td>4</td>
                    <td>file 4</td>
                    <td>Nguyễn Văn B</td>
                    <td>PNG</td>
                    <td>2011/04/25</td>
                    <td>
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#exampleModal">
                        Tải
                      </button>                        
                      <button type="button" class="btn btn-default btn-sm" data-toggle="modal btn-delete">
                        Xóa
                      </button>
                    </td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div> 

        </div>
      </div>
			<!-- /page content -->

			<%@include file="/base/footer.jsp" %>
		</div>
	</div>
	<!-- js custom -->
	<script src="resources/js/myScript.js"></script>
</body>
</html>
