<%-- 
    Document   : sample
    Created on : Oct 10, 2018, 10:32:54 PM
    Author     : DamHaiDang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="/base/meta.jsp" %>
        <title>JSP Page</title>
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <%@include file="/base/header.jsp" %>
                
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="row">


                        
                        <!-- content -->
                    </div>
                </div>
                <!-- /page content -->
                
                <%@include file="/base/footer.jsp" %>
            </div>
        </div>
    </body>
</html>
