<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <%@include file="/base/meta.jsp" %>
  <link href="resources/css/document_create.css" rel="stylesheet">
  <title>Tạo công văn trực tuyến</title>
</head>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <%@include file="/base/header.jsp" %>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="row page-content">
          <div class="x_panel">

            <div class="row form-group">
              <div class="col-md-6 div-tile">
                <input id="title" type="text" placeholder="Tiêu Đề" class="form-control" name="">
              </div>
              <div class="col-md-6 pull-right">
                <button id="document_save" class="btn btn-primary pull-right"><i class="fa fa-save">&nbsp;&nbsp; Lưu</i></button>
              </div>
            </div>

            <div class="row" id="area-document">
              <div id="toolbar-container"></div>
              <div id="editor">
                <p></p>
              </div>
            </div>

            <div class="pull-right footer">
                Phát triển phần mềm hướng dịch vụ | Nhóm 2 - KTPM 5 - K10
            </div>
          
          </div>
        </div>
      </div>
      <!-- /page content -->
      <%@include file="/base/footer.jsp" %>
    </div>
  </div>
  <!-- js custom -->
  <script src="libs/ckeditor.js"></script>
  <script src="resources/js/document_create.js"></script>
  <script src="libs/html-docx-js/FileSaver.js"></script>
  <script src="libs/html-docx-js/html-docx.js"></script>
  <script>
    DecoupledEditor
    .create(document.querySelector('#editor'))
    .then(editor => {
      const toolbarContainer = document.querySelector('#toolbar-container');

      toolbarContainer.appendChild(editor.ui.view.toolbar.element);
    })
    .catch(error => {
      console.error(error);
    });
  </script>
</body>
</html>
