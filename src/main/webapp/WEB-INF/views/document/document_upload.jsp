<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Tải lên tài liệu</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <%@include file="/base/meta.jsp" %>
  <link href="resources/css/index.css" rel="stylesheet">
  <link href="resources/css/document_upload.css" rel="stylesheet">
  <!-- select2 -->
  <link href="libs/select2/select2.min.css" rel="stylesheet">
</head>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <%@include file="/base/header.jsp" %>

      <!-- page content -->
      <div class="right_col" role="main" enctype="multipart/form-data">
        <div class="row page-content">
          <div class="x_panel">
            <%
              String message = (String) request.getAttribute("message");
              if (message != null && message.length() > 0) {
                out.print(message);
              }
            %>

            <ul id="document-type-source">
              <c:forEach var="types" items="${listType}" >
                <option value="${types}">${types}</option>
              </c:forEach>
            </ul>

            <form id="file_form" name="fileForm" method="POST" action="${pageContext.request.contextPath}/UploadFileController1" enctype="multipart/form-data">
            </form>

            <div class="contain-btn-submit"></div>            
            <div class="add-file form-group">
              <i class="fa fa-upload"></i>
            </div>

            <form id="info_file_form" method="POST" action="${pageContext.request.contextPath}/UploadFileController2">
              <input id="hd_id" type="hidden" name="id">
              <input id="hd_title" type="hidden" name="title">
              <input id="hd_type" type="hidden" name="type">
              <input id="hd_type_detail" type="hidden" name="type_detail">
              <input id="hd_describe" type="hidden" name="description">
            </form>

            <div class="pull-right footer">
                Phát triển phần mềm hướng dịch vụ | Nhóm 2 - KTPM 5 - K10
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->

      <%@include file="/base/footer.jsp" %>
    </div>
  </div>
  <!-- select2 -->
  <script src="libs/select2/select2.min.js"></script>
  <!-- js custom -->
  <script src="resources/js/document_upload.js"></script>
</body>
</html>
