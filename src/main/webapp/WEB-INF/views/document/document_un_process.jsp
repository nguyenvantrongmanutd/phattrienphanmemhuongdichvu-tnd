<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <%@include file="/base/meta.jsp" %>
  <link href="resources/css/document_not_process.css" rel="stylesheet">
  <!-- select2 -->
  <link href="libs/select2/select2.min.css" rel="stylesheet">
  <title>Công văn chưa xử lý</title>
</head>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <%@include file="/base/header.jsp" %>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="row page-content">
          <div class="x_panel">

            <div class="flash-message">
<!--               <p class="alert alert-danger">danger <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
              <p class="alert alert-success">
                <%
                String message = (String) request.getAttribute("message");
                if (message != null && message.length() > 0) {
                  out.print(message);
                }
                %>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              </p> -->
            </div>
            <a type="button" href="#" id="delete" class="btn btn-sm btn-default"><i class="fa fa-trash"></i>
              Xóa
            </a>

            <a type="button" href="#" id="download-all" class="btn btn-sm btn-success" disabled><i class="fa fa-download"></i>
              Tải Về
            </a>

            <a type="button" href="#" id="send-all" class="btn btn-sm btn-success" data-toggle="modal" disabled><i class="fa fa-send-o"></i>
              Gửi
            </a>

            <a type="button" href="DocumentCreate" class="btn btn-sm btn-success pull-right"><i class="fa fa-plus"></i>
              Tạo Tài Liệu
            </a>

            <a type="button" href="DocumentUpload" class="btn btn-sm btn-success pull-right"><i class="fa fa-upload"></i>
              Tải Lên Tài Liệu
            </a>

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search-plus"></i>
              Tìm Kiếm Nâng Cao
            </button>

            <!--Search modal -->
            <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <div class="form-horizontal">

                      <div class="form-group">
                        <label class="control-label col-sm-3" for="title">Tiêu đề</label>
                        <div class="col-sm-9">
                          <input id="title" placeholder="Tên Tài liệu" class="form-control input-sm" type="text">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="middle-name" class="control-label col-sm-3">Loại Tài Liệu: </label>
                        <div class="col-sm-9">
                          <select id="document-type" class="form-control input-sm">
                            <option ></option>
                            <c:forEach items="${typeList}" var="type">
                              <c:if test="${type != selected}">
                                <option value="${type}">${type}</option>
                              </c:if>
                            </c:forEach>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="middle-name" class="control-label col-sm-3">Từ ngày: </label>
                        <div class="col-sm-9">
                          <div class="input-group date datepicker" style="margin-bottom: 0px;" data-provide="datepicker">
                            <input id="start-date" readonly="readonly" type="text" class="form-control input-sm">
                            <div class="input-group-addon">
                              <span class="glyphicon glyphicon-th"></span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="middle-name" class="control-label col-sm-3">Tới ngày: </label>
                        <div class="col-sm-9">
                          <div class="input-group date datepicker" data-provide="datepicker">
                            <input id="end-date" readonly="readonly" type="text" class="form-control input-sm">
                            <div class="input-group-addon">
                              <span class="glyphicon glyphicon-th"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="button" id="btn-search" class="btn btn-sm btn-primary">Tìm Kiếm</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- send modal -->
            <div class="modal fade" id="sendModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label for="deparment-receive" class="control-label col-sm-3">Phòng ban nhận: </label>
                        <div class="col-sm-9">
                          <select id="deparment-receive" class="form-control input-sm">
                            <option ></option>
                            <c:forEach items="${typeList}" var="type">
                              <c:if test="${type != selected}">
                                <option value="${type}">${type}</option>
                              </c:if>
                            </c:forEach>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="employer-receive" class="control-label col-sm-3">Người nhận: </label>
                        <div class="col-sm-9">
                          <select id="employer-receive" class="form-control input-sm" disabled multiple="multiple">
                            <option >1</option>
                            <option >2</option>
                            <option >3</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-3" for="message">Tin nhắn: </label>
                        <div class="col-sm-9">
                          <textarea style="height: 100px;" id="message" placeholder="Nội dung ..." class="form-control"></textarea>
                        </div>
                      </div>

                      <div class="modal-footer">
                        <button id="btn-send-close" type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="button" id="btn-send" class="btn btn-sm btn-primary">Gửi</button>
                      </div>
                    </div> 
                  </div>
                </div>
              </div> 
            </div>

            <!-- datatable client -->
            <table id="documents-un-process-table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th><input type="checkbox" id="check-all"></th>
                  <th>STT</th>
                  <th>Tiêu Đề</th>
                  <th>Loại Công Văn</th>
                  <th>Ngày tải lên</th>
                  <th>Người tải lên</th>
                </tr>
              </thead>
            </table>

            <div class="pull-right footer">
              Phát triển phần mềm hướng dịch vụ | Nhóm 2 - KTPM 5 - K10
            </div>
          </div>
        </div> 
      </div> 
      <!-- page content -->
      
    </div>
  </div>

  <%@include file="/base/footer.jsp" %>
  <!-- select2 -->
  <script src="libs/select2/select2.min.js"></script>
  <!-- js custom -->
  <script src="resources/js/document_not_processed.js"></script>
</body>
</html>
