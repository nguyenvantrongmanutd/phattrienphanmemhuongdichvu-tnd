$(document).ready(function () {
  
  $('.datepicker').datepicker();

  var table = $('#datatable').DataTable({
    // "pagingType": "simple",
    'order': [1, 'asc'],
    'columnDefs': [
      {
        'targets': [0, 7],
        'orderable': false
      },
      // {
      //   "targets": 0,
      //   "render": function(data, type, row){
      //     return '<input type="checkbox" class="check-row-product" name="'+ data +'" value="'+ data +'" >';
      //   }
      // },            
      // { 
      //   "targets": 4,
      //   "render": function(data, type, row){
      //     return '<img src="'+ data +'" alt="Image">';
      //   }
      // }
    ],
    "columns": [
      { "width": "3%" },
      { "width": "5%" },
      { "width": "30%" },
      { "width": "15%" },
      { "width": "10%" },
      { "width": "15%" },
      { "width": "10%" }
    ],
  });

  arrCheckedTemplate = [];
  $(document).on('click','#check-all',function() {
    if($(this).is(":checked")) {
      $('.checkbox').each(function () {
        arrCheckedTemplate.push($(this).val());
      });

      $(".checkbox").prop("checked", true);
    } else{
      $(".checkbox").prop("checked", false);
    arrCheckedTemplate = [];
    }
  });  

  $(document).on('click','.checkbox',function() {
    $('.checkbox').each(function () {
      arrCheckedTemplate.push($(this).val());
    });

  });

  $(document).on('click','#delete-all',function() {
    arrCheckedTemplate = [];

    $('.checkbox').each(function () {
      if($(this).is(":checked")) {
        arrCheckedTemplate.push($(this).val());
      }
    });

    if (arrCheckedTemplate.length == 0) {
      alert("Choose document to delete!");

    } else {
      confirm("Are you sure?");
    }
  });

  $(document).on('click','.btn-delete',function() {
    confirm("Are you sure?");
  });

  $(document).on('click','#download-all',function() {
    $('.checkbox').each(function () {
      if($(this).is(":checked")) {
        arrCheckedTemplate.push($(this).val());
      }
    });

    if (arrCheckedTemplate.length == 0) {
      alert("Choose document to download!");

    } else {
      alert("download");
    }
  });

});