$(document).ready(function () {
    $(document).on("click", "#document_save", function () {
        let header = "<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n</head>\n";
        let html = "<body>" + $(".ck-editor__editable").html() + "</body>";
        let title = $("#title").val();
        let converted = htmlDocx.asBlob(header + html);
        saveAs(converted, title + '.docx');
    });
});
