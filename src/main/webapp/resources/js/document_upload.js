$(document).ready(function() {
  var file_no = 0;
  var no = 0;
  var document_type_option = $("#document-type-source").html();

  $(document).on("click", ".add-file", function() {
    file_no++;
    let html = "<input id=\"file-"+file_no+"\" class=\"file-hidden file-"+file_no+"\" name=\"file[]\" file-no=\""+file_no+"\" type=\"file\">";
    // $(html).insertBefore(this);
    $("#file_form").append(html);
    $(".file-"+file_no).click();
  });

  //remove a row field
  $(document).on("click", ".fa-remove", function() {
    no--;
    if (no == 0) {
      $(".fa-upload").animate({"font-size": "50px"}); 
      $("#btn-register").hide(500);
      setTimeout(function() {
        $("#btn-register").remove();
      }, 501);
    }
    let val = $(this).attr("file-no");
    $(".row-file-"+val).hide(500);
    setTimeout(function() {
      $(".row-file-"+val).remove();
    }, 501);
    $(".file-"+val).remove();
  });

  //genarate fields contain file upload infomation
  $(document).on("change", ".file-hidden", function() {
    $(".file-hidden").each(function() {
      if ($(this).val() == "") {
        $(this).remove();
      }
    });
    
    if (no == 0) {
      $(".contain-btn-submit").html("<button id=\"btn-register\" class=\"btn btn-primary\">Tải lên</button>");
      $("#btn-register").hide().show(500);
    }
    no++;
    
    let html = "<div class=\"form-group row row-file-"+file_no+"\">";
    html += "<div class=\"col-sm-4\">";
    html += "<input id=\"title-"+file_no+"\" class=\"form-control title input-file-"+file_no+"\" name=\"documentTitle[]\" required input-no=\""+file_no+"\" type=\"text\" placeholder=\"Tiêu đề\">";
    html += "</div>";
    
    html += "<div class=\"col-sm-2\">";
    html += "<select required id=\"type-"+file_no+"\" file-no="+file_no+" class=\"form-control document-type\" name=\"documentType[]\">";
    html += "<option style=\"font-style: italic;\" value='' hidden selected=\"selected\">-- Loại công văn --</option>";
    html += document_type_option;
    html += "</select>";
    html += "</div>";
    
    html += "<div class=\"col-sm-2\">";
    html += "<select disabled id=\"type-detail-"+file_no+"\" class=\"form-control input-sm type-detail\" name=\"documentTypeDetail[]\" multiple=\"multiple\" required=\"required\">";
    html += "</select>";
    html += "</div>";
    
    html += "<div class=\"col-sm-3\">";
    html += "<textArea style=\"height: 32.5px;\" name=\"documentDescription[]\" class=\"form-control col-sm-12 description\" placeholder=\"Mô tả cho tài liệu\">";
    html += "</textArea>";
    html += "</div>";
    
    html += "<i class=\"fa fa-2x fa-remove\" file-no=\""+file_no+"\"></i>";
    html += "</div>";
    $(html).insertAfter(".contain-btn-submit").hide().show(500);
    $(".row-file-"+file_no).hide(); 
    $(".row-file-"+file_no).show(500);
    $(".fa-upload").animate({"font-size": "30px"}); 

    let val = $(".file-"+file_no).val();
    val = val.slice(val.lastIndexOf("\\")+1);
    $(".input-file-"+file_no).val(val); 

    //select2
    $("#type-detail-"+file_no).select2({
      placeholder: '-- Loại cụ thể --'
    });
  });

  //event when chossing document type
  $(document).on("change", ".document-type", function() {
    let file_no_let = $(this).attr("file-no");
    let type = $(this).val();
    $.ajax({
      url: "DocumentTypeJson",
      type: "POST",
      data: {
        DocumentType: type
      },
      success: function(data) {
        let length = data.length;
        let option_html = "";
        for (var i = 0; i < length; i++) {
            option_html += "<option value=\""+data[i]+"\">"+data[i]+"</option>";
        }
         $("#type-detail-"+file_no_let).html(option_html);
         $("#type-detail-"+file_no_let).removeAttr("disabled");
      },
    });
  });
  
  //focus in textaria
  $(document).on("focusin", ".description", function() {
    $(this).animate({"height": "140px"});
  });
  
  //focus out textaria
  $(document).on("focusout", ".description", function() {
    $(this).animate({"height": "30px"});
  });

  //click button upload
  $(document).on("click", "#btn-register", function(e) {
    e.preventDefault();

    //format all of title
    let document_title = "";
    $(".title").each(function() {
      document_title += $(this).val()+";";
    });
    document_title = document_title.substring(0, document_title.length-1);
    $("#hd_title").val(document_title);
    
    //format all of type
    let document_type = "";
    $(".document-type").each(function() {
      document_type += $(this).val()+";";
    });
    document_type = document_type.substring(0, document_type.length-1);
    $("#hd_type").val(document_type);

    //format all of type detail
    let type_detail = "";
    $(".type-detail").each(function() {
      let value = $(this).val();
      type_detail += value.toString()+";";
    });
    type_detail = type_detail.substring(0, type_detail.length-1);
    $("#hd_type_detail").val(type_detail);

    //format all of description
    let description = "";
    $(".description").each(function() {
      description += $(this).val()+";";
    });
    description = description.substring(0, description.length-1);
    $("#hd_describe").val(description);

    $('div.progress > div.progress-bar').css({ "width": "100%" }); 
    var form = document.forms.namedItem("fileForm");
    var formData = new FormData(form);
    var request = new XMLHttpRequest();
    request.open("POST", "UploadFileController1");
    request.send(formData);

    let id = "";
    request.onload = function () {
      if (request.readyState == request.DONE) {
        if (request.status == 200) {
          $("#hd_id").val(request.response);
          $("#info_file_form").submit();
        }
      }
    };
  });

});