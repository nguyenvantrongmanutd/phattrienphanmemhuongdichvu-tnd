$(document).ready(function () {
  $('.datepicker').datepicker();
  $('.js-example-basic-multiple').select2();
  $("#employer-receive").select2({
    placeholder: '-- Loại cụ thể --'
  });

  var arrCheckedTemplate = [];
  //check all of checkbox in table
  $(document).on('click','#check-all',function() {
    $("#download-all").attr("disabled", "disabled");
    arrCheckedTemplate = [];
    if($(this).is(":checked")) {
      $('.check-row').each(function () {
        arrCheckedTemplate.push($(this).val());
      });

      $(".check-row").prop("checked", true);
      $("#send-all").removeAttr("disabled");
      $("#send-all").attr("data-target", "#sendModal");


    } else{
      $("#send-all").attr("disabled", "disabled");
      $("#send-all").removeAttr("data-target");

      $(".check-row").prop("checked", false);
      arrCheckedTemplate = [];
    }
  });  

  //click checkbox in table
  $(document).on('click','.check-row',function() {
    arrCheckedTemplate = [];
    $('.check-row').each(function () {
      if($(this).is(":checked")) {
        arrCheckedTemplate.push($(this).val());
      }
    });
    
    let arr = "(";
    let length = arrCheckedTemplate.length;
    if (length == 0) {
      $("#send-all").attr("disabled", "disabled");
      $("#send-all").removeAttr("data-target");
      $("#download-all").attr("disabled", "disabled");

    } else if (length == 1) {
      $("#download-all").removeAttr("disabled");

    } else {
      $("#download-all").attr("disabled", "disabled");
      $("#send-all").removeAttr("disabled");
      $("#send-all").attr("data-target", "#sendModal");
    }

    for(i=0; i<length; i++) {
      if (i == length-1) {
        arr += arrCheckedTemplate[i]+")"; 
      } else {
        arr += arrCheckedTemplate[i]+","; 
      }
    }
  });

  $(document).on("change", "#deparment-receive", function() {
    let val = $("#deparment-receive").val();
    $("#employer-receive").removeAttr("disabled");
    $.ajax({
      url: "document/loadUser",
      type: "post",
      data: {
        deparment_id: val,
      },
      success: function(data) {
        console.log(data);

      }, 
      error: function() {
      }
    });
  });

  //click button send
  $(document).on("click", "#btn-send", function() {
    let deparment_receive = $("#deparment-receive").val();
    let employer_receive = $("#employer-receive").val();
    let message = $("#message").val();

    arrCheckedTemplate = [];
    $('.check-row').each(function () {
      if($(this).is(":checked")) {
        arrCheckedTemplate.push($(this).val());
      }
    });

    let arr = "(";
    let length = arrCheckedTemplate.length;
    for(i=0; i<length; i++) {
      if (i == length-1) {
        arr += arrCheckedTemplate[i]+")"; 
      } else {
        arr += arrCheckedTemplate[i]+","; 
      }
    }

    $.ajax({
      url: "document/send",
      type: "post",
      data: {
        idArr: arr,
        deparment_receive: deparment_receive,
        employer_receive: employer_receive,
        message: message,
      },
      success: function() {
        $().showAlert('Gửi thành công', 'success');
        $("#btn-send-close").click();
        table.ajax.reload();
      }, 
      error: function() {
        $().showAlert('Gửi không thành công', 'danger');
        $("#btn-send-close").click();
        table.ajax.reload();
      }
    });
  });

  //delete
  $(document).on('click','#delete',function() {

    $('.check-row').each(function () {
      if($(this).is(":checked")) {
        arrCheckedTemplate.push($(this).val());
      }
    });
    
    if (arrCheckedTemplate.length == 0) {
      alert("Choose document to delete!");

    } else {
      let arr = "(";
      let length = arrCheckedTemplate.length;
      for(i=0; i<length; i++) {
        if (i == length-1) {
          arr += arrCheckedTemplate[i]+")"; 
        } else {
          arr += arrCheckedTemplate[i]+","; 
        }
      }
      if(confirm('Bạn có muốn xóa không?')) {
        $.ajax({
          url: "document/delete",
          type: "post",
          data: {
            idArr: arr
          },
          success: function() {
            $().showAlert('Xóa thành công', 'success');
            table.ajax.reload();
            $("#send-all").attr("disabled", "disabled");
            $("#send-all").removeAttr("data-target");
            $("#download-all").attr("disabled", "disabled");
          }, 
          error: function() {
            $().showAlert('Xóa không thành công', 'danger');
          }
        });
      };
    }
  });

  $(document).on("click", "#btn-search", function() {
    table.ajax.reload();
  });

  var table = $('#documents-un-process-table').DataTable({
    "serverSide": true,
    "searching": false,
    "pageLength": 10,
    "pagingType": "simple",
    "order": [],
    "ajax": {
      url: 'documents/un_process/datatable',
      type: 'post',
      loading: true,
      data: function(data) {
        data.search = {
          "documentTitle": $("#title").val(),
          "documentType": $("#document-type").val(),
          "documentStartDate": $("#start-date").val(),
          "documentEndDate": $("#end-date").val(),
        }
      }
    },
    'columnDefs': [
    {
      "targets": 0,
      "orderable": false,
      "render": function(data, type, row){

        return '<input type="checkbox" name="'+ data +'" class="check-row" value="'+data+'" document-id="'+ data +'" >';
      }
    },
    ],
    "columns": [
    {
      "data": "documentId",
      "width": "4%"
    },
    {
      "data": "documentId",
      "width": "6%"
    },
    {
      "data": "documentTitle",
    },
    {
      "data": "documentType",
      "width": "15%"
    },
    {
      "data": "documentCreatedTime",
      "width": "15%"
    },
    {
      "data": "documentCreatedBy",
      "width": "18%"
    },
    ], 
  });

  $(document).on('click', '#download-all', function () {
    $('.checkbox').each(function () {
      if ($(this).is(":checked")) {
        arrCheckedTemplate.push($(this).val());
      }
    });

    if (arrCheckedTemplate.length == 1) {
      $.ajax({
        url: "document/download",
        type: "post",
        data: {
          id: arrCheckedTemplate[0]
        },
        success: function() {
        }, 
        error: function() {
        }
      });
    }
  });
});