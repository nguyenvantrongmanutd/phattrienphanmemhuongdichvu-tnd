$(document).ready(function() {
  $.fn.showAlert = function(text, type) {
    $('.alert').remove();
    var type = typeof type !== 'undefined' ? type : 'success';
    type = type == 'error' ? 'danger' : type;
    var alert = $('<p class="alert alert-' + type +'">' + text + '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>');
    var element = ".flash-message";
    $(element).prepend(alert);

    $(".x_panel").prepend(alert);
  };
});