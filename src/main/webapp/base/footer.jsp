<%-- 
    Document   : footer
    Created on : Oct 10, 2018, 10:26:05 PM
    Author     : DamHaiDang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- jQuery -->
<script src="libs/jquery/dist/jquery.min.js"></script>
<!-- jQuery validation -->
<script src="libs/jquery.validate.min.js"></script>
<!-- Bootstrap -->
<script src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Chart.js -->
<script src="libs/Chart.js/dist/Chart.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="libs/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- Skycons -->
<script src="libs/skycons/skycons.js"></script>
<!-- DateJS -->
<script src="libs/DateJS/build/date.js"></script>
<!-- Datatables -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<!-- Custom libs -->
<script src="libs/custom/custom.js"></script>
<!-- <script src="resources/js/myScript.js"></script> -->
<!-- Datatables -->
<script src="libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="libs/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="libs/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<!-- Custom Theme Scripts -->
<script src="resources/js/custom.min.js"></script>
<!-- datetime-picker -->
<script src="resources/js/bootstrap-datepicker.js"></script>
<!-- custom js -->
<script src="resources/js/common.js"></script>
