<%-- 
    Document   : meta
    Created on : Oct 10, 2018, 10:31:29 PM
    Author     : DamHaiDang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- bootstrap-progressbar -->
<link href="libs/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
<!-- Datatables -->
<link href="libs/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="libs/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="resources/css/custom.min.css" rel="stylesheet">
<!-- <link href="resources/css/index.css" rel="stylesheet"> -->
<link href="resources/css/common.css" rel="stylesheet">
