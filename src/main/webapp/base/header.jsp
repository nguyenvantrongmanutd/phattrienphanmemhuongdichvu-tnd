<%-- 
    Document   : header
    Created on : Oct 10, 2018, 10:29:43 PM
    Author     : DamHaiDang
    --%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% 
    if(session.getAttribute("userId")==null){
        out.print("<meta http-equiv='refresh' content='0; url=index.jsp' />");
    }
%>
<div class="progress">
  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
  </div>
</div>

<div class="main_container">
  <div class="col-md-3 left_col">
    <div class="left_col scroll-view">
    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="resources/images/img.jpg" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Xin Chào,</span>
        <h2><%=(String) session.getAttribute("userLastname")%></h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <ul class="nav side-menu">

          <li>
            <a><i class="fa fa-edit"></i> Quản Lý Giấy Tờ <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="DocumentReceive">Công Văn Đến</a></li>
              <li><a href="DocumentSend">Công Văn Đi</a></li>
            </ul>
          </li>

          <li>
            <a><i class="fa fa-plus"></i> Thêm Công Văn <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="DocumentUpload">Tải Lên</a></li>
              <li><a href="DocumentCreate">Tạo Công Văn Trực Tuyến</a></li>
            </ul>
          </li>

          <li>
            <a><i class="fa fa-desktop"></i> Tình Trạng Giấy Tờ <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="DocumentProcessed">Công Văn Đã Xử Lý</a></li>
              <li><a href="DocumentUnProcess">Công Văn Chưa Xử Lý</a></li>
            </ul>
          </li>

          <li>
            <a><i class="fa fa-user"></i> Thông Tin Cá Nhân <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="UserUpdate">Cập Nhập</a></li>
            </ul>
          </li>

          <li>
            <a><i class="fa fa-bar-chart-o"></i> Thống Kê <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="StatisticChart">Biểu Đồ</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-edit"></i> Quản Lý Tài Khoản <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="ManagerList">Danh Sách</a></li>
              <li><a href="ManagerCreateUser">Tạo Tài Khoản</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
  </div>
</div>

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="resources/images/img.jpg" alt="">
            <%=(String) session.getAttribute("userLastname")%>
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="Logout"><i class="fa fa-sign-out pull-right"></i> Đăng Xuất</a></li>
          </ul>
        </li>

      </ul>
    </nav>
  </div>
</div>
<!-- /top navigation -->
