<!DOCTYPE html>
<html>
    <head>
        <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <meta charset="utf-8">
        <title>login form</title>
        <link rel="stylesheet" type="text/css" href="bootstrap.css">
        <link rel="stylesheet" type="text/css" href="login.css">
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    </head>
    <body>
        <div class="all" style="height: 100%">
            <div class="col-md-12 col-sm-12" style="height: 20%"></div>
            <div class="col-md-7 col-sm-7 div-left text-login" style="height: 60%">
                <p style="font-size: 60px; margin-top: 35px;">Hệ Thống Quản Lý <b>Công Văn Giấy Tờ</b></p>
                <p style="font-size: 30px"><i>vndocument.com</i></p>
            </div>
            <div class="col-md-5 col-sm-5" style="height: 60%">
                <div class="col-md-2 col-sm-2" style="height: 100%; "></div>
                <div class="col-md-8 col-sm-8" style="height: 100%;">
                    <div class="col-md-4 col-sm-4"></div>
                    <div class="col-md-4 col-sm-4 img-icon">
                        <img width="100%" src="user_icon_1.png">
                    </div>
                    <div class="col-md-4 col-sm-4"></div>

                    <form action="${pageContext.request.contextPath}/CheckLogin" method="POST">
                        <div class="form-group">
                            <input class="form-control" placeholder="Tài khoản" type="text" name="username" required>
                        </div>
                        <div class="form-group ">
                            <input class="form-control" placeholder="Mật khẩu" type="password" name="password" required>
                        </div>
                        <%
                            String msg = (String) request.getAttribute("msg");
                            if (msg != null && msg != "") {
                                out.print(msg);
                            }
                        %>  
                        <button class="btn col-md-12 col-sm-12" >Đăng Nhập</button>
                    </form>
                </div>
                <div class="col-md-2 col-sm-2" style="height: 100%; "></div>
            </div>
            <div class="col-md-12 col-sm-12" style="height: 20%"></div>
        </div>
    </body>
</html>