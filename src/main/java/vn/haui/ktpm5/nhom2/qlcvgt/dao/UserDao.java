/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.haui.ktpm5.nhom2.qlcvgt.entity.UserEntity;
import vn.haui.ktpm5.nhom2.qlcvgt.util.MySQLConnector;

/**
 *
 * @author DamHaiDang
 */
public class UserDao {

    Connection conn;
    PreparedStatement ptmt;
    ResultSet rs;

    public boolean createUser(UserEntity user) {
        boolean check = false;
        try {
            conn = MySQLConnector.getMySQLConnection();
            String sql = "insert into EMPLOYEE values ()";
            int row = ptmt.executeUpdate();
            return (row > 0);
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return check;
    }

    public int checkUser(String username, String password) {
        int userId = 0;
        try {
            conn = MySQLConnector.getMySQLConnection();
            String sql = "select USER_ID from USER where USERNAME = ? and PASSWORD = ?";
            ptmt = conn.prepareStatement(sql);
            ptmt.setString(1, username);
            ptmt.setString(2, password);
            rs = ptmt.executeQuery();
            if (rs.next()) {
                userId = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return userId;
    }
    public String getUserLastName(String userId) {
        String userLastname = "user";
        try {
            conn = MySQLConnector.getMySQLConnection();
            String sql = "select USER_LASTNAME from USER where USER_ID = ?";
            ptmt = conn.prepareStatement(sql);
            ptmt.setString(1, userId);
            rs = ptmt.executeQuery();
            if (rs.next()) {
                userLastname = rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return userLastname;
    }
}
