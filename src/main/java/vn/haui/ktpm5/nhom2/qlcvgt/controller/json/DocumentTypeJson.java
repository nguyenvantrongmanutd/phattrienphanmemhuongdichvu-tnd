/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.controller.json;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vn.haui.ktpm5.nhom2.qlcvgt.common.DocumentCommon;

/**
 *
 * @author DamHaiDang
 */
@WebServlet(name = "DocumentTypeJson", urlPatterns = {"/DocumentTypeJson"})
public class DocumentTypeJson extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String type = "";
        if (request.getParameterMap().containsKey("DocumentType")) {
            type = request.getParameter("DocumentType");
        }

        if (type != null && !"".equals(type)) {
            List<String> listTypeDetail = DocumentCommon.getTypeDetails(type);
            Gson gson = new Gson();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            String listTypeDetailString = gson.toJson(listTypeDetail);
            out.print(listTypeDetailString);
            out.flush();
        }
    }

}
