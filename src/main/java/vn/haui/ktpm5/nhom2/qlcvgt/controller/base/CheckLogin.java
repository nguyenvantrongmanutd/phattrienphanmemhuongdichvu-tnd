/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.controller.base;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import vn.haui.ktpm5.nhom2.qlcvgt.dao.UserDao;

/**
 *
 * @author DamHaiDang
 */
@WebServlet(name = "CheckLogin", urlPatterns = {"/CheckLogin"})
public class CheckLogin extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDao dao = new UserDao();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        int userId = dao.checkUser(username, password);
        if (userId != 0) {
            HttpSession session = request.getSession();
            String userLastname = dao.getUserLastName(Integer.toString(userId));
            session.setAttribute("userId", userId);
            session.setAttribute("userLastname", userLastname);
            request.getRequestDispatcher("/WEB-INF/views/document/document_upload.jsp").forward(request, response);
        } else {
            request.setAttribute("msg", "<div class=\"alert alert-danger\" role=\"alert\">\n"
                    + "          Tài khoản hoặc mật khẩu sai\n"
                    + "          </div>");
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

}
