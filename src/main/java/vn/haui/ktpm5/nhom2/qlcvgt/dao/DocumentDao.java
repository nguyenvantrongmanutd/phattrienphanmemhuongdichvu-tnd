/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.haui.ktpm5.nhom2.qlcvgt.constant.DocumentConstant;
import vn.haui.ktpm5.nhom2.qlcvgt.entity.DocumentEntity;
import vn.haui.ktpm5.nhom2.qlcvgt.util.MySQLConnector;

/**
 *
 * @author DamHaiDang
 */
public class DocumentDao {

    Connection conn;
    PreparedStatement ptmt;
    ResultSet rs;

    public List<DocumentEntity> getListUnProcessDocument(String column, String order, String start, String length, String docTitle, String docType, String docStartDate, String docEndDate) {
        try {
            List<DocumentEntity> list = new ArrayList<DocumentEntity>();
            conn = MySQLConnector.getMySQLConnection();
            String sql = "select * from DOCUMENT where DOCUMENT_STATUS = 0 ";
            if (docTitle != null && !"".equals(docTitle)) {
                sql += " and DOCUMENT_TITLE like '%" + docTitle + "%'";
            }
            if (docType != null && !"".equals(docType)) {
                sql += " and DOCUMENT_TYPE like '%" + docType + "%'";
            }
            if (docStartDate != null && !"".equals(docStartDate) && docEndDate != null && !"".equals(docEndDate)) {
                sql += " and DOCUMENT_CREATED_TIME BETWEEN '" + docStartDate + "' AND '" + docEndDate + "'";
            }
            if ("1".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL1;
            }
            if ("2".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL2;
            }
            if ("3".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL3;
            }
            if ("4".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL4;
            }
            if ("5".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL5;
            }
            if ("asc".equals(order)) {
                sql += " asc";
            }
            if ("desc".equals(order)) {
                sql += " desc";
            }
            if (!"".equals(start)) {
                sql += (" limit " + start + "," + length);
            }
            System.out.println(sql);
            ptmt = conn.prepareStatement(sql);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                DocumentEntity d = new DocumentEntity();
                d.setDocumentId(rs.getInt("DOCUMENT_ID"));
                d.setDocumentTitle(rs.getString("DOCUMENT_TITLE"));
                d.setDocumentType(rs.getString("DOCUMENT_TYPE"));
                d.setDocumentCreatedBy(rs.getString("DOCUMENT_CREATED_BY"));
                d.setDocumentCreatedTime(rs.getString("DOCUMENT_CREATED_TIME"));
                list.add(d);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return new ArrayList<DocumentEntity>();
    }

    public List<DocumentEntity> searchDocument(String docId, String docTitle, String docType, String docStatus, String docStartDate, String docEndDate) {
        try {
            List<DocumentEntity> list = new ArrayList<DocumentEntity>();
            conn = MySQLConnector.getMySQLConnection();
            String sql = "select * from DOCUMENT where 1=1 ";
            if (docId != null && !"".equals(docId)) {
                sql += " and DOCUMENT_ID = " + docId;
            }
            if (docTitle != null && !"".equals(docTitle)) {
                sql += " and DOCUMENT_TITLE = " + docTitle;
            }
            if (docType != null && !"".equals(docType)) {
                sql += " and DOCUMENT_TYPE = " + docType;
            }
            if (docStatus != null && !"".equals(docStatus)) {
                sql += " and DOCUMENT_STATUS = " + docStatus;
            }
            if (docStartDate != null && !"".equals(docStartDate) && docEndDate != null && !"".equals(docEndDate)) {
                sql += " DOCUMENT_CREATED_TIME BETWEEN '" + docStartDate + "' AND '" + docEndDate + "'";
            }
            System.out.println(sql);
            ptmt = conn.prepareStatement(sql);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                DocumentEntity d = new DocumentEntity();
                d.setDocumentId(rs.getInt("DOCUMENT_ID"));
                d.setDocumentTitle(rs.getString("DOCUMENT_TITLE"));
                d.setDocumentType(rs.getString("DOCUMENT_TYPE"));
                d.setDocumentCreatedBy(rs.getString("DOCUMENT_CREATED_BY"));
                d.setDocumentCreatedTime(rs.getString("DOCUMENT_CREATED_TIME"));
                list.add(d);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return new ArrayList<DocumentEntity>();
    }

    public boolean createDocument(DocumentEntity d) {
        try {
            conn = MySQLConnector.getMySQLConnection();
            String sql = "insert into DOCUMENT("
                    + "DOCUMENT_TITLE," //1
                    + " DOCUMENT_TYPE," //2
                    + " DOCUMENT_URL," //3
                    + " DOCUMENT_STATUS," //4
                    + " DOCUMENT_DESCRIPTION," //5
                    + " DOCUMENT_CREATED_BY," //6
                    + " DOCUMENT_CREATED_TIME," //7
                    + " USER_SENT_ID) values " //8
                    + "(?,?,?,?,?,?,?,?)";
            ptmt.setString(1, d.getDocumentTitle());
            ptmt.setString(2, d.getDocumentType());
            ptmt.setString(3, d.getDocumentUrl());
            ptmt.setInt(4, d.getDocumentStatus());
            ptmt.setString(5, d.getDocumentDescription());
            ptmt.setString(6, d.getDocumentCreatedBy());
            ptmt.setString(7, d.getDocumentCreatedTime());
            ptmt.setInt(8, d.getUserSentId());
            return ptmt.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    public List<String> getAllType() {
        try {
            List<String> listType = new ArrayList<String>();
            conn = MySQLConnector.getMySQLConnection();
            String sql = "select DOCUMENT_TYPE from DOCUMENT group by DOCUMENT_TYPE";
            ptmt = conn.prepareStatement(sql);
            rs = ptmt.executeQuery();
            while (rs.next()) {
                listType.add(rs.getString(1));
            }
            return listType;
        } catch (SQLException ex) {
            Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return new ArrayList<String>();
    }

    public int insertDocument(DocumentEntity doc) {
        try {
            conn = MySQLConnector.getMySQLConnection();
            String sql = "insert into DOCUMENT("
                    + "DOCUMENT_TITLE, " //1
                    + "DOCUMENT_TYPE, " //2
                    + "DOCUMENT_URL, "//3
                    + "DOCUMENT_STATUS, " //4
                    + "DOCUMENT_DESCRIPTION, " //5
                    + "DOCUMENT_CREATED_BY, " //6
                    + "DOCUMENT_TYPE_DETAIL, " //7
                    + "DOCUMENT_CREATED_TIME " // END
                    + ") "
                    + "values (?,?,?,?,?,?,?, CURRENT_DATE())";
            System.out.println("Insert Document: " + sql);
            ptmt = conn.prepareStatement(sql);
            ptmt.setString(1, doc.getDocumentTitle());
            ptmt.setString(2, doc.getDocumentType());
            ptmt.setString(3, doc.getDocumentUrl());
            ptmt.setInt(4, doc.getDocumentStatus());
            ptmt.setString(5, doc.getDocumentDescription());
            ptmt.setString(6, doc.getDocumentCreatedBy());
            ptmt.setString(7, doc.getDocumentTypeDetail());
            return ptmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public int deleteDocuments(String ids) {
        try {
            conn = MySQLConnector.getMySQLConnection();
            String sql = "UPDATE DOCUMENT set DOCUMENT_STATUS = 3 where DOCUMENT_ID in " + ids;
            System.out.println("Delete DOCUMENT: " + sql);
            ptmt = conn.prepareStatement(sql);
            return ptmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DocumentDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public Integer countDocuments(String column, String order, String start, String length, String docTitle, String docType, String docStartDate, String docEndDate) {
        try {
            Integer row = 0;
            conn = MySQLConnector.getMySQLConnection();
            String sql = "select count(*) from DOCUMENT where DOCUMENT_STATUS = 0 ";
            if (docTitle != null && !"".equals(docTitle)) {
                sql += " and DOCUMENT_TITLE like '%" + docTitle + "%'";
            }
            if (docType != null && !"".equals(docType)) {
                sql += " and DOCUMENT_TYPE like '%" + docType + "%'";
            }
            if (docStartDate != null && !"".equals(docStartDate) && docEndDate != null && !"".equals(docEndDate)) {
                sql += " and DOCUMENT_CREATED_TIME BETWEEN '" + docStartDate + "' AND '" + docEndDate + "'";
            }
            if ("1".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL1;
            }
            if ("2".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL2;
            }
            if ("3".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL3;
            }
            if ("4".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL4;
            }
            if ("5".equals(column)) {
                sql += " order by " + DocumentConstant.TABLE_UNPROCESS_COL5;
            }
            if ("asc".equals(order)) {
                sql += " asc";
            }
            if ("desc".equals(order)) {
                sql += " desc";
            }
            if (!"".equals(start)) {
                sql += (" limit " + start + "," + length);
            }
            System.out.println(sql);
            ptmt = conn.prepareStatement(sql);
            rs = ptmt.executeQuery();
            if (rs.next()) {
                row = rs.getInt(1);
            }
            return row;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 0;
    }

    public Integer getGeneratedInsertKey(DocumentEntity doc) {
        int generatedKey = 0;
        try {
            conn = MySQLConnector.getMySQLConnection();
            String sql = "insert into DOCUMENT ("
                    + "DOCUMENT_TITLE, " //1
                    + "DOCUMENT_URL, " //2
                    + "DOCUMENT_STATUS"
                    + ") values (?,?,0)";
            ptmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ptmt.setString(1, doc.getDocumentTitle());
            ptmt.setString(2, doc.getDocumentUrl());
            ptmt.execute();
            rs = ptmt.getGeneratedKeys();
            if (rs.next()) {
                generatedKey = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return generatedKey;
    }

    public int updateDocument(DocumentEntity doc) {
        int row = 0;
        try {
            conn = MySQLConnector.getMySQLConnection();
            String sql = "update DOCUMENT set "
                    + "DOCUMENT_TYPE = ?, " //1
                    + "DOCUMENT_TYPE_DETAIL = ?, " //2
                    + "DOCUMENT_DESCRIPTION = ?, " //3
                    + "DOCUMENT_CREATED_BY = ?," //4
                    + "DOCUMENT_TITLE = ?, " //5
                    + "DOCUMENT_CREATED_TIME = CURRENT_DATE() "
                    + "where DOCUMENT_ID =  " + doc.getDocumentId();
            ptmt = conn.prepareStatement(sql);
            ptmt.setString(1, doc.getDocumentType());
            ptmt.setString(2, doc.getDocumentTypeDetail());
            ptmt.setString(3, doc.getDocumentDescription());
            ptmt.setString(4, doc.getDocumentCreatedBy());
            ptmt.setString(5, doc.getDocumentTitle());
            row = ptmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return row;
    }
}
