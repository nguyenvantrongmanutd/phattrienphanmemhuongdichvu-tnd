/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.controller.json;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vn.haui.ktpm5.nhom2.qlcvgt.bussiness.DocumentBusiness;
import vn.haui.ktpm5.nhom2.qlcvgt.entity.DocumentEntity;
import vn.haui.ktpm5.nhom2.qlcvgt.entity.base.Datatable;

/**
 *
 * @author DamHaiDang
 */
@WebServlet(name = "DocumentUnprocessJson", urlPatterns = {"/documents/un_process/datatable"})
public class DocumentUnprocessJson extends HttpServlet {

    DocumentBusiness business;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String column = "";
        String order = "";
        String draw = request.getParameter("draw");
        String start = request.getParameter("start");
        String length = request.getParameter("length");
        String docTitle = "";
        String docType = "";
        String docStartDate = "";
        String docEndDate = "";
        if (request.getParameterMap().containsKey("search[documentTitle]")) {
            docTitle = request.getParameter("search[documentTitle]");
        }
        if (request.getParameterMap().containsKey("search[documentType]")) {
            docType = request.getParameter("search[documentType]");
        }
        if (request.getParameterMap().containsKey("search[documentStartDate]")) {
            docStartDate = request.getParameter("search[documentStartDate]");
        }
        if (request.getParameterMap().containsKey("search[documentEndDate]")) {
            docEndDate = request.getParameter("search[documentEndDate]");
        }
        if (request.getParameterMap().containsKey("order[0][column]")) {
            column = request.getParameter("order[0][column]");
            order = request.getParameter("order[0][dir]");
        }
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        business = new DocumentBusiness();
        List<DocumentEntity> list = business.getListUnprocessDocument(column, order, start, length, docTitle, docType, docStartDate, docEndDate);
//        int recordsTotal = business.getListUnprocessDocument("", "", "", "", "", "", "", "").size();
//        int recordsFiltered = business.getListUnprocessDocument("", "", start, length, "", "", "", "").size();
        int recordsTotal = business.countDocuments("", "", "", "", "", "", "", "");
        int recordsFiltered = business.countDocuments("", "", start, length, "", "", "", "");
        Datatable datatable = new Datatable(draw, recordsTotal, recordsTotal, list);
        Gson gson = new Gson();
        String jsonString = gson.toJson(datatable);
        out.print(jsonString);
        out.flush();
        System.out.println(jsonString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
