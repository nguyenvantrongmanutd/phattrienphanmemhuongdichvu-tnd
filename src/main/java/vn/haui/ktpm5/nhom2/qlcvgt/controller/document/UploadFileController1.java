/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.controller.document;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import vn.haui.ktpm5.nhom2.qlcvgt.bussiness.DocumentBusiness;
import static vn.haui.ktpm5.nhom2.qlcvgt.constant.DocumentConstant.UPLOAD_DIRECTORY;
import vn.haui.ktpm5.nhom2.qlcvgt.entity.DocumentEntity;

/**
 *
 * @author DamHaiDang
 */
@WebServlet(name = "UploadFileController1", urlPatterns = {"/UploadFileController1"})
public class UploadFileController1 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String listFileUploaded = "";
        if (ServletFileUpload.isMultipartContent(request)) {
            DocumentBusiness business = new DocumentBusiness();
            try {
                List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);
                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        String name = new File(item.getName()).getName();
                        String url = UPLOAD_DIRECTORY + File.separator + name;
                        item.write(new File(url));
                        // insert Document into DB
                        DocumentEntity doc = new DocumentEntity();
                        doc.setDocumentTitle(name);
                        doc.setDocumentUrl(url);
                        Integer idKey = business.getGeneratedInsertKey(doc);
                        listFileUploaded += idKey + ";";
                    }
                }

                //File uploaded successfully
            } catch (Exception ex) {
//                request.setAttribute("message", "File Upload Failed due to " + ex);
            }
        }
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print(listFileUploaded.substring(0, listFileUploaded.length()-1));
//        out.print("100;101;102");
    }
}
