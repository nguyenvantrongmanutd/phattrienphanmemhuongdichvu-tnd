/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.bussiness;

import java.util.List;
import vn.haui.ktpm5.nhom2.qlcvgt.dao.DocumentDao;
import vn.haui.ktpm5.nhom2.qlcvgt.entity.DocumentEntity;

/**
 *
 * @author DamHaiDang
 */
public class DocumentBusiness {

    DocumentDao dao;

    public List<DocumentEntity> getListUnprocessDocument(String column, String order, String start, String length, String docTitle, String docType, String docStartDate, String docEndDate) {
        dao = new DocumentDao();
        return dao.getListUnProcessDocument(column, order, start, length, docTitle, docType, docStartDate, docEndDate);
    }

    public List<String> getDocumentTypeList() {
        dao = new DocumentDao();
        return dao.getAllType();
    }

    public int uploadDocument(DocumentEntity doc) {
        dao = new DocumentDao();
        return dao.insertDocument(doc);
    }
    
    public int deleteDocuments(String ids){
        dao = new DocumentDao();
        return dao.deleteDocuments(ids);
    }

     public Integer countDocuments(String column, String order, String start, String length, String docTitle, String docType, String docStartDate, String docEndDate) {
        dao = new DocumentDao();
        return dao.countDocuments(column, order, start, length, docTitle, docType, docStartDate, docEndDate);
    }
     
     public Integer getGeneratedInsertKey(DocumentEntity doc){
         dao = new DocumentDao();
         return dao.getGeneratedInsertKey(doc);
     }
     
     public  Integer updateDocument(DocumentEntity doc){
         dao = new DocumentDao();
         return dao.updateDocument(doc);
     }
}
