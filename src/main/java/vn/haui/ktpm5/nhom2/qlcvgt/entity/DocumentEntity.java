/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author DamHaiDang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentEntity {

    private Integer documentId;
    private String documentTitle;
    private String documentUrl;
    private String documentType;
    private String documentTypeDetail;
    private Integer documentStatus;
    private String documentCreatedBy;
    private Integer documentCreatedById;
    private String documentDescription;
    private String documentCreatedTime;
    private String documentUpdatedBy;
    private Integer userSentId;
    private String userReceiveId;

}
