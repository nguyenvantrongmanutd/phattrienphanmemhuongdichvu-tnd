/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.controller.document;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vn.haui.ktpm5.nhom2.qlcvgt.common.DocumentCommon;

/**
 *
 * @author DamHaiDang
 */
@WebServlet(name = "DocumentUploadController", urlPatterns = {"/DocumentUpload"})
public class DocumentUploadController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<String> listType = DocumentCommon.getTypes();
        request.setAttribute("listType", listType);
        RequestDispatcher requestDispatcher;
        requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/document/document_upload.jsp");
        requestDispatcher.forward(request, response);
    }
}
