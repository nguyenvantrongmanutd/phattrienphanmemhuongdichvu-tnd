/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.entity.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author DamHaiDang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Datatable {
    private String draw;
    private int recordsTotal;
    private int recordsFiltered;
    private Object data;
}
