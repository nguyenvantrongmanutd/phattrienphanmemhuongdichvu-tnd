/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author DamHaiDang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentEntity {

    private int departmentId;
    private String departmentName;
    private String departmentCreatedBy;
    private String departmentUpdatedBy;
}
