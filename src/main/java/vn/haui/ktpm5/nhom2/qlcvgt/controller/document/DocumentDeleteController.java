/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.controller.document;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vn.haui.ktpm5.nhom2.qlcvgt.bussiness.DocumentBusiness;

/**
 *
 * @author DamHaiDang
 */
@WebServlet(name = "DocumentDeleteController", urlPatterns = {"/document/delete"})
public class DocumentDeleteController extends HttpServlet {

    DocumentBusiness business;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        business = new DocumentBusiness();
        String ids = request.getParameter("idArr");
        if (ids != null && !"".equals(ids));
        {
            business.deleteDocuments(ids);
        }

        RequestDispatcher requestDispatcher;
        requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/document/document_un_process.jsp");
        requestDispatcher.forward(request, response);
    }

}
