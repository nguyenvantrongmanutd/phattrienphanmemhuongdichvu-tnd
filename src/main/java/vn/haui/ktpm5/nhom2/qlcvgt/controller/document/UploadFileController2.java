/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.controller.document;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vn.haui.ktpm5.nhom2.qlcvgt.bussiness.DocumentBusiness;
import vn.haui.ktpm5.nhom2.qlcvgt.entity.DocumentEntity;

/**
 *
 * @author DamHaiDang
 */
@WebServlet(name = "UploadFileController2", urlPatterns = {"/UploadFileController2"})
@MultipartConfig
public class UploadFileController2 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DocumentBusiness business = new DocumentBusiness();
        String idList = request.getParameter("id");
        String titleList = request.getParameter("title");
        String typeList = request.getParameter("type");
        String typeDetailList = request.getParameter("type_detail");
        String descriptionList = request.getParameter("description");
        if (idList != null && titleList != null && typeList != null && typeDetailList != null && descriptionList != null) {
            String[] idArr = idList.split(";");
            String[] titleArr = titleList.split(";");
            String[] typeArr = typeList.split(";");
            String[] typeDetailArr = typeDetailList.split(";");
            String[] descriptionArr = descriptionList.split(";");
            for (int i = 0; i < idArr.length; i++) {
                DocumentEntity doc = new DocumentEntity();
                doc.setDocumentId(Integer.parseInt(idArr[i]));
                doc.setDocumentTitle(titleArr[i]);
                doc.setDocumentType(typeArr[i]);
                doc.setDocumentTypeDetail(typeDetailArr[i]);
                doc.setDocumentDescription(descriptionArr[i]);
                business.updateDocument(doc);
            }
            request.setAttribute("message", "Uploaded successful!");
        } else {
            request.setAttribute("message", "Upload failed");
        }
        request.getRequestDispatcher("/WEB-INF/views/document/document_upload.jsp").forward(request, response);
    }
}
