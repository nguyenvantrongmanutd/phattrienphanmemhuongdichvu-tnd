/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.constant;

import lombok.Data;

/**
 *
 * @author DamHaiDang
 */

@Data
public class DocumentConstant {
    // datatable unprocess
    public static final String TABLE_UNPROCESS_COL1 = "DOCUMENT_ID";
    public static final String TABLE_UNPROCESS_COL2 = "DOCUMENT_TITLE";
    public static final String TABLE_UNPROCESS_COL3 = "DOCUMENT_TYPE";
    public static final String TABLE_UNPROCESS_COL4 = "DOCUMENT_CREATED_BY";
    public static final String TABLE_UNPROCESS_COL5 = "DOCUMENT_CREATED_TIME";
    
    // folder file upload
    public static final String UPLOAD_DIRECTORY = "/mnt/Lig/Workspace/PTPMHDV/phattrienphanmemhuongdichvu-tnd/src/main/webapp/WEB-INF/files";
    
    //type of document
    public static final String DOC_TYPE_THONG_BAO = "Thông báo";
    public static final String DOC_TYPE_BAO_CAO = "Báo cáo";
    public static final String DOC_TYPE_TO_TRINH = "Tờ trình";
    public static final String DOC_TYPE_CONG_VAN = "Công văn";
    public static final String DOC_TYPE_BIEN_BAN = "Biên bản";
    public static final String DOC_TYPE_HANH_CHINH = "Hành chính";
    
    //detail type of document
    public static final String DOC_TYPE_DETAIL_THONG_BAO_TIN_TUC = "Tin tức";
    public static final String DOC_TYPE_DETAIL_THONG_BAO_QUYET_DINH = "Quyết định";
    public static final String DOC_TYPE_DETAIL_THONG_BAO_QUY_DINH = "Quy định";
    
    public static final String DOC_TYPE_DETAIL_BAO_CAO_KE_HOACH = "Kế hoạch";
    public static final String DOC_TYPE_DETAIL_BAO_CAO_KET_QUA = "Kết quả";
    public static final String DOC_TYPE_DETAIL_BAO_CAO_VAN_DE = "Vấn đề";
    public static final String DOC_TYPE_DETAIL_BAO_CAO_HIEN_TRANG = "Hiện trạng";
    public static final String DOC_TYPE_DETAIL_BAO_CAO_QUY_TRINH = "Quy trình";
    
    public static final String DOC_TYPE_DETAIL_TO_TRINH_DE_NGHI_PHE_DUYET = "Đề nghị phê duyệt";
    public static final String DOC_TYPE_DETAIL_TO_TRINH_VAN_BAN_QUY_PHAM = "Văn bản quy phạm";
    public static final String DOC_TYPE_DETAIL_TO_TRINH_THAM_DINH = "Thẩm định";
    public static final String DOC_TYPE_DETAIL_TO_TRINH_DANG_KY = "Đăng ký";
    public static final String DOC_TYPE_DETAIL_TO_TRINH_XIN_CHAP_THUAN = "Xin chấp thuận ";
    
    public static final String DOC_TYPE_DETAIL_CONG_VAN_GIAI_THICH = "Giải thích";
    public static final String DOC_TYPE_DETAIL_CONG_VAN_MOI_HOP_MOI_DU = "Mời họp - mời dự";
    public static final String DOC_TYPE_DETAIL_CONG_VAN_CHAN_CHINH_NHAC_NHO = "Chấn chỉnh - nhắc nhở";
    public static final String DOC_TYPE_DETAIL_CONG_VAN_DE_NGHI = "Đề nghị";
    public static final String DOC_TYPE_DETAIL_CONG_VAN_PHUC_DAP = "Phúc đáp";
    
    public static final String DOC_TYPE_DETAIL_BIEN_BAN_BAN_GIAO_CONG_VIEC = "Bàn giao công việc";
    public static final String DOC_TYPE_DETAIL_BIEN_BAN_GIAO_NHAN_TAI_SAN = "Giao nhận tài sản";
    public static final String DOC_TYPE_DETAIL_BIEN_BAN_NGHIEM_THU = "Nghiệm thu";
    public static final String DOC_TYPE_DETAIL_BIEN_BAN_XU_PHAT = "Xử phạt";
            
    public static final String DOC_TYPE_DETAIL_HANH_CHINH_GIAY_GIA_HAN = "Giấy gia hạn";
    public static final String DOC_TYPE_DETAIL_HANH_CHINH_GIAY_LINH_TIEN_MAT = "Giấy lĩnh tiền mặt";
    public static final String DOC_TYPE_DETAIL_HANH_CHINH_GIAY_BIEN_NHAN = "Giấy biên nhận";
    public static final String DOC_TYPE_DETAIL_HANH_CHINH_GIAY_CHUNG_NHAN = "Giấy chứng nhận";
    public static final String DOC_TYPE_DETAIL_HANH_CHINH_GIAY_UY_QUYEN = "Giấy ủy quyền";
    
}
