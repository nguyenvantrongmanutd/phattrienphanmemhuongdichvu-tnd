/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.Part;
import vn.haui.ktpm5.nhom2.qlcvgt.constant.DocumentConstant;

/**
 *
 * @author DamHaiDang
 */
public class DocumentCommon {

    public static List<String> getTypeDetails(String type) {
        List<String> list = new ArrayList<String>();
        if (DocumentConstant.DOC_TYPE_THONG_BAO.equals(type)) {
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_HIEN_TRANG);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_KET_QUA);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_KE_HOACH);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_QUY_TRINH);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_VAN_DE);
        } else if (DocumentConstant.DOC_TYPE_BAO_CAO.equals(type)) {
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_KE_HOACH);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_KET_QUA);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_VAN_DE);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_HIEN_TRANG);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BAO_CAO_QUY_TRINH);
        } else if (DocumentConstant.DOC_TYPE_TO_TRINH.equals(type)) {
            list.add(DocumentConstant.DOC_TYPE_DETAIL_TO_TRINH_DANG_KY);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_TO_TRINH_DE_NGHI_PHE_DUYET);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_TO_TRINH_THAM_DINH);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_TO_TRINH_VAN_BAN_QUY_PHAM);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_TO_TRINH_XIN_CHAP_THUAN);
        } else if (DocumentConstant.DOC_TYPE_CONG_VAN.equals(type)) {
            list.add(DocumentConstant.DOC_TYPE_DETAIL_CONG_VAN_CHAN_CHINH_NHAC_NHO);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_CONG_VAN_DE_NGHI);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_CONG_VAN_GIAI_THICH);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_CONG_VAN_MOI_HOP_MOI_DU);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_CONG_VAN_PHUC_DAP);
        } else if (DocumentConstant.DOC_TYPE_BIEN_BAN.equals(type)) {
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BIEN_BAN_BAN_GIAO_CONG_VIEC);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BIEN_BAN_GIAO_NHAN_TAI_SAN);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BIEN_BAN_NGHIEM_THU);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_BIEN_BAN_XU_PHAT);
        } else if (DocumentConstant.DOC_TYPE_HANH_CHINH.equals(type)) {
            list.add(DocumentConstant.DOC_TYPE_DETAIL_HANH_CHINH_GIAY_BIEN_NHAN);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_HANH_CHINH_GIAY_CHUNG_NHAN);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_HANH_CHINH_GIAY_GIA_HAN);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_HANH_CHINH_GIAY_LINH_TIEN_MAT);
            list.add(DocumentConstant.DOC_TYPE_DETAIL_HANH_CHINH_GIAY_UY_QUYEN);
        }
        return list;
    }

    public static List<String> getTypes() {
        List<String> list = new ArrayList<String>();
        list.add(DocumentConstant.DOC_TYPE_THONG_BAO);
        list.add(DocumentConstant.DOC_TYPE_BAO_CAO);
        list.add(DocumentConstant.DOC_TYPE_TO_TRINH);
        list.add(DocumentConstant.DOC_TYPE_CONG_VAN);
        list.add(DocumentConstant.DOC_TYPE_BIEN_BAN);
        list.add(DocumentConstant.DOC_TYPE_HANH_CHINH);
        return list;
    }

    public static String getValueFromPart(Part part) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(part.getInputStream(), "UTF-8"));
        StringBuilder value = new StringBuilder();
        char[] buffer = new char[1024];
        for (int length = 0; (length = reader.read(buffer)) > 0;) {
            value.append(buffer, 0, length);
        }
        return value.toString();
    }
}
