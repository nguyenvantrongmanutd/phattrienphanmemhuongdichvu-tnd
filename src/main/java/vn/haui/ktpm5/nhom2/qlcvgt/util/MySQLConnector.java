/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author DamHaiDang
 */
public class MySQLConnector {
    public static Connection getMySQLConnection() throws SQLException,
            ClassNotFoundException {
//        String hostName = "server.stu18.vn";
//        String dbName = "vndocume_db";
//        String userName = "vndocume_a@a";
//        String password = "vndocume_a@a";
        String hostName = "localhost";
        String dbName = "VNDOCUMENT_DB";
        String userName = "hbstudent";
        String password = "hbstudent";

        return getMySQLConnection(hostName, dbName, userName, password);
    }

    public static Connection getMySQLConnection(String hostName, String dbName,
            String userName, String password) throws SQLException,
            ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName;
        Connection conn = DriverManager.getConnection(connectionURL, userName,
                password);
        return conn;
    }
}
