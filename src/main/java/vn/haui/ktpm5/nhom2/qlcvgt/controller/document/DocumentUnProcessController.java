/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.haui.ktpm5.nhom2.qlcvgt.controller.document;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vn.haui.ktpm5.nhom2.qlcvgt.bussiness.DocumentBusiness;

/**
 *
 * @author DamHaiDang
 */
@WebServlet(name = "DocumentUnProcess", urlPatterns = {"/DocumentUnProcess"})
public class DocumentUnProcessController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DocumentBusiness business = new DocumentBusiness();
        List<String> typeList = business.getDocumentTypeList();
        request.setAttribute("typeList", typeList);
        RequestDispatcher requestDispatcher;
        requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/document/document_un_process.jsp");
        requestDispatcher.forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
