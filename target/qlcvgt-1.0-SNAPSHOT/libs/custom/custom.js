$(document).ready(function () {
  
  if($('.datepicker').length > 0){
    // init datepicker
    $('.datepicker').datepicker.dates['vn'] = {
    days: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
    daysShort: ['T2','T3','T4','T5','T6','T7','CN'],
    daysMin: ['T2','T3','T4','T5','T6','T7','CN'],
    months: ['Tháng 1','Tháng 2','Tháng 3','Tháng 4','Tháng 5','Tháng 6',
      'Tháng 7','Tháng 8','Tháng 9','1Tháng 0','1Tháng 1','1Tháng 2'],
    monthsShort: ['Tháng 1','Tháng 2','Tháng 3','Tháng 4','Tháng 5','Tháng 6',
      'Tháng 7','Tháng 8','Tháng 9','Tháng 10','Tháng 11','Tháng 12'],
    today: "今日",
    clear: "閉じる",
    format: "yyyy/mm/dd",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
   };

    $('.datepicker').datepicker({
        language: 'vn',
        autoclose: true
    });     
  }

  $.extend( $.fn.dataTable.defaults, {
//      language: {
//      "emptyTable" : "Không Có Dữ Liệu",
//      "sProcessing":   "Đang Xử Lý...",
//      "sLengthMenu":   " Số Hàng: _MENU_",
//      "sZeroRecords":  "データはありません。",
//      "sInfo":         " Tổng: _TOTAL_ | Từ _START_  Tới _END_",
//      "sInfoEmpty":    " 0 件中 0 から 0 まで表示",
//      "sInfoFiltered": "（全 _MAX_ 件より抽出）",
//      "sInfoPostFix":  "",
//      "sSearch":       "Tìm Kiếm:",
//      "sUrl":          "",
//      "oPaginate": {
//          "sFirst":    "Đầu Tiên",
//          "sPrevious": "Trang Trước",
//          "sNext":     "Tiếp Theo",
//          "sLast":     "Cuối Cùng"
//      }
//    }

    language: {
      "emptyTable" : "Không Có Dữ Liệu",
      "sProcessing":   "Đang Xử Lý...",
      "sLengthMenu":   " Số Hàng: _MENU_",
      "sZeroRecords":  " Không có bản ghi ",
      "sInfo":         " Tổng: _TOTAL_ | Từ _START_  Tới _END_",
      "sInfoEmpty":    "_TOTAL_ ",
      "sInfoFiltered": "(Trong số _MAX_ bản ghi）",
      "sInfoPostFix":  "",
      "sSearch":       "Tìm Kiếm:",
      "sUrl":          "",
      "oPaginate": {
          "sFirst":    "Đầu Tiên",
          "sPrevious": "Trang Trước",
          "sNext":     "Tiếp Theo",
          "sLast":     "Cuối Cùng"
      }
    }
  });
  
});